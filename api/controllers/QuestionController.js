/**
 * QuestionController
 *
 * @description :: Server-side logic for managing questions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

 	newQuestion: function(req,res){
 		res.view();
 	},

 	create: function(req, res, next){
 		Question.create( req.params.all(), function questionCreated (err, question){
 			if(err) return next(err);

 			res.redirect('/question/showQuestion/'+ question.id);
 		});
 	},

	showQuestion: function(req, res, next){
		Question.findOne(req.param('id'), function foundQuestion (err, question) {
			if(err) return next(err);
			if(!question) return next();
			res.view({
				question: question
			});
		});
	}
 };