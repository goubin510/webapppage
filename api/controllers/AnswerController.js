/**
 * AnswerController
 *
 * @description :: Server-side logic for managing answers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


module.exports = {

	newAnswer: function(req,res){
 		res.view();
 	},

	create: function(req, res, next){
		Answer.create( req.params.all(), function answerCreated (err, answer){
			if(err) return next(err);

			res.redirect('/answer/showAnswer/'+ answer.id);
		});
  },

	showAnswer: function(req, res, next){
		Answer.findOne(req.param('id'), function foundAnswer (err, answer) {
			if(err) return next(err);
			if(!answer) return next();
			res.view({
				answer: answer
			});
		});
	}
};

